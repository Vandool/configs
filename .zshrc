# To set locale
export LC_TIME="en_DE.UTF-8"
export LC_MONETARY="en_DE.UTF-8"

# pip
export PATH="$PATH:/var/data/python/bin"

# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Don't forget what these are
# gdu: an awesome overview of the disk usage sorted by folders with highest usage
# procs: a replacement for the ps 
# tokei: A program that prints out statistics about code.


# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
#if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
#  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
#fi

# ugly solution for the keyborad compose key
GTK_IM_MODULE=xim
export $GTK_IM_MODULE


# For more configurations visit /usr/share/zshrc

# Manjaro zsh 
# Use powerline
USE_POWERLINE="true"
# Source manjaro-zsh-configuration
if [[ -e /usr/share/zsh/manjaro-zsh-config ]]; then
  source /usr/share/zsh/manjaro-zsh-config
fi
# Use manjaro zsh prompt
if [[ -e /usr/share/zsh/manjaro-zsh-prompt ]]; then
  source /usr/share/zsh/manjaro-zsh-prompt
fi

# Rust Replacements 
alias cat="bat" # requires bat
alias vim="neovide" # Neovide gui rust vim
alias nvim="neovim" # Neovide gui rust vim

alias ls="exa --oneline"
alias la="exa -la"
alias ll="exa --long --tree --level=2"


# Add some aliases
alias setkwin="neovide ~/.config/kwinrc"
alias setzsh="neovide ~/.zshrc"
alias setbash="neovide ~/.bashrc"
alias setvim="neovide ~/.config/nvim/"
alias setluavim="neovide /home/arvand/.config/nvim/lua/config.lua" 
alias setonedrive="neovide ~/.config/onedrive/config"

alias ...="cd ../.."

alias cin="xclip -sel c"
alias cout="xclip -sel c -o"
alias getpwd="pwd | tr -d '\n' | cin && echo '$PWD copied to clipboard'"


# Functions for managing files and folder
# Dependent on: fzf

# Fancy cd, you can search for the directory you're looking for
# Replaced 'find' with 'fd'
fcd() {
    cd "$(fd -Htd | fzf)"
}

open() {
    xdg-open "$(fd -tf | fzf)"
}

# find directory, remove the './' at the beginning, trim the '\n' in the end and copy
# to the clipboard
getd() {
    fd -atd | fzf | tr -d '\n' | xclip -sel c
}

# find file, remove the './' at the beginning, trim the '\n' in the end and copy
# to the clipboard
getf() {
    fd -atf | fzf | tr -d '\n' | xclip -sel c
}


# Shortcuts for folders of KIT
kit() {
    cd /home/arvand/OneDrive/KIT
}
icpc() { 
    kit
    cd 2_SS22/BasisPraktikumICPC
}
algo() {
    kit
    cd 2_SS22/'Algorithm I'
}
la2() {
    kit
    cd 2_SS22/LA
}
swt() {
    kit
    cd 2_SS22/'Softwaretechnik I'
}
ti() {
    kit 
    cd 2_SS22/Technische_Informatik
}

# Open terminal decorations
clsCounter=2
prefixIcon='>'
prefix=$prefixIcon
for i in 1 2; do prefix+=$prefixIcon  done

# Fonts and their max width 
#toiletFont="speed"
#clsMax=8
toiletFont="smslant"
clsMax=20

clear() {
    if [ $clsCounter -lt $clsMax ];
    then
        prefix+=$prefixIcon
        clsCounter=$((clsCounter + 1))
    fi

    /usr/bin/clear
    toilet -o -f $toiletFont "${prefix}ARVAND" | gay
}

# Let's beautify'
# -o: overlap
# -s, -S, -k, -W, -o       render mode (default, force smushing,
#                          kerning, full width, overlap)

toilet -o -f $toiletFont "${prefix}ARVAND" | gay

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh


# To allow instant prompt
typeset -g POWERLEVEL9K_INSTANT_PROMPT=quiet

